"""
Smartly adjust GNOME Night Light

By decreasing the temperature when it is disabled, it will be enabled.
By increasing it above the maximum temperature, it will be disabled.
"""


MINIMUM_TEMPERATURE = 1700
MAXIMUM_TEMPERATURE = 4700


import subprocess
import argparse

parser = argparse.ArgumentParser(
    description='Smartly adjust GNOME Night Light',
)
parser.add_argument('-r', '--relative', action='store_true')
parser.add_argument('temperature', type=int)


def get_enabled() -> bool:
    """Get whether night light is enabled."""
    
    output = subprocess.check_output([
        'gsettings',
        'get',
        'org.gnome.settings-daemon.plugins.color',
        'night-light-enabled',
    ])

    output = bytes.decode(output, encoding='UTF-8').strip()
    return output == 'true'


def set_enabled(enabled: bool) -> None:
    """Set whether night light is enabled."""
    subprocess.check_output([
        'gsettings',
        'set',
        'org.gnome.settings-daemon.plugins.color',
        'night-light-enabled',
        str(enabled).lower(),
    ])


def get_temperature() -> int:
    """Get the current temperature."""
    
    output = subprocess.check_output([
        'gsettings',
        'get',
        'org.gnome.settings-daemon.plugins.color',
        'night-light-temperature',
    ])

    output = bytes.decode(output, encoding='UTF-8').strip()
    type_, _, value = output.partition(" ")

    return int(value)


def set_temperature(temperature: int) -> None:
    """Set the current temperature."""

    temperature = min(max(MINIMUM_TEMPERATURE, temperature), MAXIMUM_TEMPERATURE)

    subprocess.check_output([
        'gsettings',
        'set',
        'org.gnome.settings-daemon.plugins.color',
        'night-light-temperature',
        str(temperature),
    ])


def main():
    args = parser.parse_args()

    # Calculate the absolute temperature
    temperature = args.temperature
    if args.relative:
        temperature += get_temperature()


    # Always disable above maximum
    if temperature > MAXIMUM_TEMPERATURE:
        set_enabled(False)
        set_temperature(MAXIMUM_TEMPERATURE)

    # Only enable if decreasing when disabled
    elif not get_enabled():
        set_enabled(True)
        set_temperature(MAXIMUM_TEMPERATURE)

    # Otherwise, adjust the temperature accordingly
    else:
        set_temperature(temperature)


if __name__ == '__main__':
    main()